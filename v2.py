import sys

par = sys.argv[1:]


def count_ones(str):
    count = 0
    for i in range(len(str)):
        if str[i] == "1": count += 1
    return count


def remove_repeated(array):
    seen = []
    for i in array:
        if i not in seen:
            seen.append(i)

    return seen


def entrada(table, bits, sorted=False):
    initial = 0
    final = []
    final_sorted = [[] for i in range(bits + 1)]

    # print(final_sorted)
    for i in range(len(table)):
        if table[i] == 1:
            binar = bin(initial)[2:]
            if len(binar) < bits:
                binar = ('0') * (bits - len(binar)) + binar
            final.append(binar)
            final_sorted[count_ones(binar)].append(binar)
        initial += 1

    final_sorted = [x for x in final_sorted if x != []]
    if sorted:
        return final_sorted
    else:
        return final


def comparator(array, main_table):
    matter_pos = []
    match = []
    for char in range(len(array)):
        if (array[char] == "0") or (array[char] == "1"):
            matter_pos.append(char)

    for number in range(len(main_table)):
        counter = 0
        for digit in range(len(main_table[number])):
            if digit in matter_pos:
                if main_table[number][digit] == array[digit]:
                    counter += 1

        if counter == len(matter_pos):
            match.append(main_table[number])
        else:
            match.append(' ' * bits)
    # print(array, "matches with", match)
    return match


def binary_to_letter(s):
    out = ''
    c = 'a'
    more = False
    n = 0
    for i in range(len(s)):
        if not more:
            if s[i] == '1':
                out = out + c
            elif s[i] == '0':
                out = out + c + '\''

        if more:
            if s[i] == '1':
                out = out + c + str(n)
            elif s[i] == '0':
                out = out + c + str(n) + '\''
            n += 1
        # conditions for next operations
        if c == 'z' and more == False:
            c = 'A'
        elif c == 'Z':
            c = 'a'
            more = True

        elif not more:
            c = chr(ord(c) + 1)

    return out


# table_original = [1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1]
# table_original = [1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0]
# table_original = [1,1,1,1,0,1,1,1,0,1,0,1,1,1,0,1,1,0,1,1,0,1,1,1,1,0,0,1,0,1,1,0]
# table_original = [1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,0,0,1,1,0,1,1,1,0,1,1,0,0,1,0,0,0]
# table_original = [1,1,1,1,1,1,0,1,1,0,1,1,1,0,1,1]
# table_original = [1,1,0,1,1,0,1,1]
# table_original = [1,1,1,1,1,0,0,1,0,0,0,0,0,0,1,1,1,1,1,0,0,1,1,1,0,0,0,1,1,0,1,1,1,1,1,1,1,0,1,1,0,1,1,1,0,0,1,1,0,0,1,1,1,1,0,1,1,0,1,0,1,0,1,1]
# table_original = [1,0,0,1,0,1,1,0,1,1,0,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,1,1,1,0,0,0]
# table_original = [0,1,1,0,0,0,0,1,0,1,0,0,1,0,0,1]
# table_original = [0,0,1,1,0,1,1,1,1,1,0,1,1,1,1,0,1,0,0,0,1,0,1,0,1,0,0,1,0,1,1,1,0,1,0,1,0,1,0,0,1,1,0,1,1,0,0,0,0,0,0,0,1,0,1,0,0,1,0,1,0,1,1,0]


table_original = [1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0]
print(len(table_original))
bits = 4
table = entrada(table_original, bits)
table_numbers = table
print(table)
print()


def match(array1, array2):
    count = 0
    position = 0
    numbers = []
    for i in range(len(array1)):
        if array1[i] != array2[i]:
            count += 1
            position = i
        if count > 1:
            return False, array1

    # return True, position, [int(array1, 2), int(array2, 2)]
    return True, replace(array1, position, str=True)


def replace(array, pos, str=False):
    if str:
        array = list(array)
        for i in range(len(array)):

            if i == pos:
                array[i] = '-'

        final = ""
        for i in range(len(array)):
            final += array[i]
        return final

    if not str:
        final_array = [i for i in array]
        final_array[pos] = "-"
        return final_array


def possibilities(array, interacao=0, result=True):
    used = []
    final = []
    matched = []

    for item in range(len(array)):
        for reitem in range(len(array)):
            if array[item] != array[reitem]:
                matching = match(array[item], array[reitem])
                if matching[0]:
                    if interacao != 0:
                        if array[item] not in matched:
                            matched.append(array[item])
                        if array[reitem] not in matched:
                            matched.append(array[reitem])
                    else:
                        if array[item] not in matched:
                            matched.append(array[item])
                        if array[reitem] not in matched:
                            matched.append(array[reitem])

                    if matching[1] not in used:
                        # print(matching)
                        final.append(matching[1])
                        used.append(matching[1])
                else:
                    pass

    not_matched = []
    if interacao != 0:
        # print("matched", matched)
        for i in range(len(array)):
            if array[i] not in matched:
                not_matched.append(array[i])

        # print("not_matched", not_matched)
        # print()
    else:
        for i in range(len(array)):
            if array[i] not in matched:
                not_matched.append(array[i])

    return final, not_matched


def column(array):
    resultante = []
    for i in range(len(array[0])):
        coluna = []
        for f in range(len(array)):
            # print(f,i)
            # print(matriz[f][i])
            coluna.append(array[f][i])

        resultante.append(coluna)

    return resultante


def essencial(array):
    prime_pos = []
    pos = 0
    for i in range(len(array)):
        count = 0
        for item in range(len(array[i])):
            if array[i][item] == '    ':
                count += 1
            if array[i][item] != '    ':
                pos = item
        if count == len(array[i]) - 1:
            prime_pos.append([count, pos])
    return prime_pos


def check_all_in_line(array, line):
    for i in range(len(array[line])):
        if array[line][i] != ' ' * bits:
            if checker[i] != 'x' * bits:
                checker[i] = 'x' * bits


def petrick(array, columns, checker):
    # if essencial_ones:
    #     return [essencial_ones[0]]

    final = []
    if ' ' * bits not in checker:
        return None

    for lines in range(len(array)):
        if lines in used:
            pass
        else:
            count = 0
            for item in range(len(array[lines])):
                if array[lines][item] != ' ' * bits:
                    if checker[item] == ' ' * bits:
                        count += 1

            if lines not in used:
                final.append([count, {lines}])

            # print("line", lines, "has", count)
    # print(sorted(final, reverse=True))
    return sorted(final, reverse=True)


count = 0
primes = []
while True:
    if not table[0]:
        break

    if count > 0:
        table = possibilities(table[0], count)
        primes.append(table[1])
    else:
        table = possibilities(table, count)
        primes.append(table[1])
    count += 1

# MULTIPLE LISTS TO SINGLE LIST
final = []
for i in primes:
    final += i

primes = final
print("primes", primes)
print()

final_work = []
for i in range(len(primes)):
    found = comparator(primes[i], table_numbers)
    final_work.append(found)

# PRINTS PETRICK'S TABLE
for i in range(len(final_work)):
    print(primes[i], final_work[i])
columns = column(final_work)
checker = [" " * bits for i in range(len(columns))]
print("-" * bits, checker)

count = 0
essencial_ones = essencial(columns)
used = []
# CHECK ALL IMPORTANT ONES
for i in range(len(essencial_ones)):
    check_all_in_line(final_work, essencial_ones[i][1])
    used.append(essencial_ones[i][1])

while True:
    if (not ' ' * bits in checker) or count > 100:
        break

    possibilities = petrick(final_work, columns, checker)
    if list(possibilities):
        check_all_in_line(final_work, list(possibilities[0][1])[0])
        used.append(list(possibilities[0][1])[0])
    count += 1

# FINAL RESULT
final_result = ""

print("\nThe Petrick's method was able to fill the following collumns:")
print('-' * bits, checker)
used = remove_repeated(used)
for i in range(len(used)):
    if i == len(used) - 1:
        final_result += binary_to_letter(primes[used[i]])
    else:
        final_result += binary_to_letter(primes[used[i]]) + " + "
print()
print(len(used), "values")
print(final_result)
print()
print("final primes", primes, "has", len(primes))
